<?php

namespace ContextualCode\LegacyPreviewSiteAccessMatcherBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeLegacyPreviewSiteAccessMatcherBundle extends Bundle {
    public function getLegacyExtensionsNames() {
        return array('cc_preview_siteaccess_matcher');
    }
}
