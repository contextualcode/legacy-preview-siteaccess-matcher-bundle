<?php
/**
 * @package contextualcode/legacy-preview-siteaccess-matcher-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    24 July 2018
 **/

class ccPreviewSiteAccessMatcherServerFunctionsAutosave extends ezjscServerFunctionsAutosave
{
    /**
     * Saves the draft and generates the preview of this draft.
     * @see self::saveDraft()
     *
     * @param array $args array( content object id, version number, locale code )
     * @return array
     */
    static public function saveDraftPreview( $args )
    {
        $result = self::saveDraft( $args );
        $object = eZContentObject::fetch( (int)$args[0] );
        $tpl = eZTemplate::factory();
        $tpl->setVariable( 'object', $object );
        $tpl->setVariable( 'version', $object->version( (int)$args[1] ) );
        $tpl->setVariable( 'locale', eZLocale::instance( $args[2] ) );
        $siteaccessList = self::getSiteaccessList( $args[2], $object );
        if ( empty( $siteaccessList ) )
        {
            $siteaccessList = array(
                eZINI::instance( 'site.ini' )->variable(
                    'SiteSettings', 'DefaultAccess'
                )
            );
        }

        $defaultSiteaccess = ccPreviewSiteAccessMatcherEvents::getMatchingSiteAccess((int)$args[0], (int)$args[1]);
        if ($defaultSiteaccess === null) {
            $defaultSiteaccess = current( $siteaccessList );
        }

        $tpl->setVariable( 'default_siteaccess', $defaultSiteaccess );
        $tpl->setVariable( 'siteaccess_list', $siteaccessList );

        $result['preview'] = $tpl->fetch( 'design:content/ajax_preview.tpl' );
        return $result;
    }
}
